#This function can truncate float without round-off
def truncate_float(number,decimal=-1):
    if decimal < 0:
        return number
    elif decimal == 0:
        return int(number)
    else:
        m = 10 ** decimal
        return int(number * m) / m
    
print(truncate_float(1.2345678))
print(truncate_float(1.2345678,0))
print(truncate_float(1.2345678,1))
print(truncate_float(1.2345678,2))
print(truncate_float(1.2345678,3))
print(truncate_float(1.234567847823234325543254673264653264532645326453265432,3))
print(truncate_float(1.234567847823234325543254673264653264532645326453265432,16)) #max limit 16